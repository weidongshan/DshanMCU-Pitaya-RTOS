#include "drv_sysclk.h"
#include "drv_systick.h"
#include "drv_timer.h"
#include <dev_io.h>
#include <dev_net.h>
#include <stdio.h>
#include <string.h>
#include "FreeRTOS.h"
#include "task.h"

extern void vStartMQTTTasks(uint16_t usTaskStackSize, UBaseType_t uxTaskPriority);
extern void vStartLEDTasks(uint16_t usTaskStackSize, UBaseType_t uxTaskPriority);
extern void vStartKeyTasks(uint16_t usTaskStackSize, UBaseType_t uxTaskPriority);

int main(void)
{
    SystemClock_Config();
    Drv_SysTick_Config();
    Drv_TIM_BaseInit();
        
    ptIODev dbgoutDev = NULL;
    dbgoutDev = IODev_GetDev(DBGOUT);
    if(dbgoutDev != NULL)
    {
        dbgoutDev->Init(dbgoutDev);
    }
    
    vStartMQTTTasks(512, 10);
    vStartLEDTasks(128, 1);
    vStartKeyTasks(128, 2);
    vTaskStartScheduler();
    
    while(1)
    {
    }
    
    return 0;
}

void SoftReset(void)
{
    __set_FAULTMASK(1);
    NVIC_SystemReset();
}

