/* Standard includes. */
#include <stdio.h>
#include <string.h>

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#include "MQTTClient.h"
#include "dev_io.h"

extern QueueHandle_t xKeyQueue;
extern TaskHandle_t ledTaskHandle;

extern void SoftReset(void);

const static char mqttHostUrl[] = "";
const static char clientID[] = "";
const static char username[] = "";
const static char password[] = "";

const static char LedTopic[] = "";
const static char KeyTopic[] = "";



void messageArrived(MessageData* data)
{
	printf("Message arrived on topic %.*s: %.*s\n", data->topicName->lenstring.len, data->topicName->lenstring.data,
		data->message->payloadlen, (char*)data->message->payload);
    
    if(strstr(data->topicName->lenstring.data, LedTopic) != 0)
    {

        if(strstr(data->message->payload, "led on") != 0)
        {
            xTaskNotify(ledTaskHandle, 1, eSetValueWithOverwrite);
        }
        else if(strstr(data->message->payload, "led off") != 0)
        {
            xTaskNotify(ledTaskHandle, 0, eSetValueWithOverwrite);
        }
    }
}

static void prvMQTTEchoTask(void *pvParameters)
{
    KeyEvent key = {0};
	/* connect to m2m.eclipse.org, subscribe to a topic, send and receive messages regularly every 1 sec */
	MQTTClient client;
	Network network;
	unsigned char sendbuf[256], readbuf[256];
	int rc = 0;
	MQTTPacket_connectData connectData = MQTTPacket_connectData_initializer;

	pvParameters = 0;
	NetworkInit(&network);
    
	MQTTClientInit(&client, &network, 10000, sendbuf, sizeof(sendbuf), readbuf, sizeof(readbuf));

	char* address = (char*)mqttHostUrl;
	if ((rc = NetworkConnect(&network, address, 1883)) != 0)
		printf("Return code from network connect is %d\r\n", rc);

#if defined(MQTT_TASK)
	if ((rc = MQTTStartTask(&client)) != pdPASS)
		printf("Return code from start tasks is %d\r\n", rc);
#endif

	connectData.MQTTVersion = 3;
	connectData.clientID.cstring = (char*)clientID;
    connectData.username.cstring = (char*)username;
    connectData.password.cstring = (char*)password;

    if ((rc = MQTTConnect(&client, &connectData)) != 0)
    {
        printf("Return code from MQTT connect is %d\r\n", rc);
        printf("Reset MCU after 5 seconds...\r\n");
        vTaskDelay(pdMS_TO_TICKS(5000));
        SoftReset();
    }
    else
    {
        printf("MQTT Connected\r\n");
    }
	

	if ((rc = MQTTSubscribe(&client, LedTopic, 0, messageArrived)) != 0)
		printf("Return code from MQTT subscribe is %d\r\n", rc);

	while (1)
	{
        if(xKeyQueue == NULL) vTaskDelay(1);
        if(xQueueReceive(xKeyQueue, (char*)&key, 10) == pdPASS)
        {
            MQTTMessage message;
            char payload[64];

            message.qos = 0;
            message.retained = 0;
            message.payload = payload;
            sprintf(payload, "Key number: %d, Press time: %d", key.num, key.time);
            message.payloadlen = strlen(payload);
            printf("%s\r\n", payload);

            if ((rc = MQTTPublish(&client, KeyTopic, &message)) != 0)
                printf("Return code from MQTT publish is %d\r\n", rc);
        }

#if !defined(MQTT_TASK)
		if ((rc = MQTTYield(&client, 100)) != 0)
			printf("Return code from yield is %d\r\n", rc);
#endif
	}

	/* do not return */
}

void vStartMQTTTasks(uint16_t usTaskStackSize, UBaseType_t uxTaskPriority)
{
	BaseType_t x = 0L;

	if(xTaskCreate(prvMQTTEchoTask,	/* The function that implements the task. */
			"MQTTEcho0",			/* Just a text name for the task to aid debugging. */
			usTaskStackSize,	    /* The stack size is defined in FreeRTOSIPConfig.h. */
			(void *)x,		        /* The task parameter, not used in this case. */
			uxTaskPriority,		    /* The priority assigned to the task is defined in FreeRTOSConfig.h. */
			NULL) == pdPASS)        /* The task handle is not used. */
            {
                printf("Create MQTT Task success.\r\n");
            }
    else
    {
        printf("Create MQTT Task failed.\r\n");
    }
}