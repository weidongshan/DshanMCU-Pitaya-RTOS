#ifndef __DRV_SYSTICK_H
#define __DRV_SYSTICK_H

int Drv_SysTick_Config(void);
unsigned int HAL_GetTick(void);
void HAL_Delay(unsigned int Delay);

#endif /* __DRV_SYSTICK_H */
