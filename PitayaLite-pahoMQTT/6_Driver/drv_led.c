#include "drv_led.h"
#include "hal_rcc.h"
#include "hal_gpio.h"

void Drv_LED_Init(void)
{
    /* 调用GPIO的HAL库对象结构体声明GPIO对象 */
    GPIO_Init_Type gpio_init;
    
    /* 使能GPIO的时钟 */
    USER_LED_GPIO_CLK_EN();
    
    /* 设置GPIO的模式 */
    gpio_init.PinMode  = GPIO_PinMode_Out_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    
    /* 选择引脚初始化 */
    gpio_init.Pins  = USER_LED_PIN;
    GPIO_Init(USER_LED_PORT, &gpio_init);
    
    
    /* 设置LED的默认状态：熄灭 */
    USER_LED(LED_OFF);
}

int Drv_LED_Write(LED_Status status)
{
    USER_LED(status);
    
    return 0;
}
