/* driver_led.h */

#ifndef __DRV_LED_H
#define __DRV_LED_H

#define USER_LED_PIN            GPIO_PIN_1
#define USER_LED_PORT           GPIOA
#define USER_LED(STATUS)        GPIO_WriteBit(USER_LED_PORT, USER_LED_PIN, STATUS)

#define USER_LED_GPIO_CLK_EN()       RCC_EnableAHB1Periphs(RCC_AHB1_PERIPH_GPIOA, true)

typedef enum{
    LED_ON  = 0,
    LED_OFF
}LED_Status;
                             
typedef enum{
    UserLed  = 0,
    Reserved
}LED_Type;

void Drv_LED_Init(void);
int Drv_LED_Write(LED_Status status);

#endif /* __DRV_LED_H */
