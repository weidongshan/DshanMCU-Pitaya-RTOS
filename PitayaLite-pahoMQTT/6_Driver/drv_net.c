#include "drv_net.h"
#include "drv_buffer.h"
#include "drv_sysclk.h"
#include "drv_systick.h"

#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"

#include <string.h>
#include <stdio.h>

#define __HAL_UART_GET_FLAG(__HANDLE__, __FLAG__)       (((__HANDLE__)->CSR & (__FLAG__)) == (__FLAG__))
#define __HAL_UART_GET_IT_FLAG(__HANDLE__, __FLAG__)    (((__HANDLE__)->ISR & (__FLAG__)) == (__FLAG__))
#define __HAL_UART_CLEAR_IT_FLAG(__HANDLE__, __FLAG__)  ((__HANDLE__)->ICR |= (__FLAG__))

static RingBuffer CmdRetBuffer;
static RingBuffer NetDataBuffer;

static void NET_UART_GPIO_Init(void);
static void NET_UART_Init(void);
static int Drv_Net_TransmitCmd(const char *cmd, const char *reply, uint16_t timeout);
void NetDataProcess_Callback(uint8_t data);

int Drv_Net_Init(void)
{
    NET_UART_GPIO_Init();
    NET_UART_Init();
    if(Drv_Buffer_Init(&CmdRetBuffer, 128) != 0) return -1;
    if(Drv_Buffer_Init(&NetDataBuffer, 1024) != 0) return -1;
    
    Drv_Net_TransmitCmd("AT+RST", "OK\r\n", 10000);
    HAL_Delay(500);
    Drv_Net_TransmitCmd("AT+CWMODE_CUR=1", "OK\r\n", 500);
    
    return 0;
}

static void NET_UART_GPIO_Init(void)
{
    GPIO_Init_Type gpio_init;
    
    UART2_GPIO_CLK_EN();
    /* PA9  - UART1_TX. */
    /* PA10 - UART1_RX. */
    gpio_init.Pins  = UART2_TX_PIN;
    gpio_init.PinMode  = GPIO_PinMode_AF_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(UART2_PORT, &gpio_init);
    GPIO_PinAFConf(UART2_PORT, gpio_init.Pins, GPIO_AF_7);
    
    gpio_init.Pins  = UART2_RX_PIN;
    gpio_init.PinMode  = GPIO_PinMode_In_Floating;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(UART2_PORT, &gpio_init);
    GPIO_PinAFConf(UART2_PORT, gpio_init.Pins, GPIO_AF_7);
}

static void NET_UART_Init(void)
{
    UART_Init_Type uart_init;
    
    NET_UART_CLK_EN();

    uart_init.ClockFreqHz   = HAL_Get_APB2_Clock();
    uart_init.BaudRate      = 115200;
    uart_init.WordLength    = UART_WordLength_8b;
    uart_init.StopBits      = UART_StopBits_1;
    uart_init.Parity        = UART_Parity_None;
    uart_init.XferMode      = UART_XferMode_RxTx;
    uart_init.HwFlowControl = UART_HwFlowControl_None;
    UART_Init(NET_UART, &uart_init);
    
    /* 使能收发中断 */
    UART_EnableInterrupts(NET_UART, UART_INT_RX_DONE|UART_IER_TXCIEN_MASK, true);
    NVIC_EnableIRQ(UART2_IRQn);
    /* 使能串口中断 */
    UART_Enable(NET_UART, true);
}

HAL_StatusTypeDef HAL_UART_Transmit(UART_Type * UARTx, uint8_t *pData, uint16_t Size)
{
    uint16_t i = 0;
    while(i < Size)
    {
        while ( 0u == (UART_STATUS_TX_EMPTY & UART_GetStatus(UARTx)) );
        UART_PutData(UARTx, (uint8_t)(pData[i]));
        i++;
    }
    return HAL_OK;
}

void UART2_IRQHandler(void)
{
    // RX irq
    if( (__HAL_UART_GET_IT_FLAG(UART2, UART_INT_RX_DONE) != 0) && (__HAL_UART_GET_FLAG(UART2, UART_INT_RX_DONE) != 0) )
    {
        __HAL_UART_CLEAR_IT_FLAG(UART2, UART_INT_RX_DONE);
        volatile uint8_t data = UART_GetData(UART2);
        
        Drv_Buffer_Write(&CmdRetBuffer, data);
        NetDataProcess_Callback(data);
    }
    // TX irq
    if(__HAL_UART_GET_IT_FLAG(UART2, UART_ISR_TXCINTF_MASK) != 0)
    {
        if(__HAL_UART_GET_FLAG(UART2, UART_INT_TX_EMPTY) != 0)
        {
            __HAL_UART_CLEAR_IT_FLAG(UART2, UART_ISR_TXCINTF_MASK);
        }
    }
}


static int Drv_Net_TransmitCmd(const char *cmd, const char *reply, uint16_t timeout)
{
    uint8_t i = 0;
    char buf[128] = {0};
    strcat(buf, cmd);
    if(strstr(buf, "\r\n") == NULL)
    {
        strcat(buf, "\r\n");
    }
    Drv_Buffer_Clean(&CmdRetBuffer);
    HAL_UART_Transmit(NET_UART, (uint8_t *)buf, strlen(buf));
    memset(buf, 0, 128);
    while(timeout != 0)
    {
        if(Drv_Buffer_Read(&CmdRetBuffer, (uint8_t*)&buf[i]) == 0)
        {
            i = (i+1)%128;
            
            if(strstr(buf, reply) != 0)
            {
//                printf("%s\r\n", buf);
                return 0;
            }
        }
        else
        {
            timeout--;
            HAL_Delay(1);
        }
    }
//    printf("%s\r\n", buf);
    return -1;
}

int Drv_Net_TransmitSocket(const char *socket, int len, int timeout)
{
    uint8_t i = 0;
    char buf[64] = {0};
    char cmd[16] = {0};
    sprintf(cmd, "AT+CIPSEND=%d\r\n", len);
    HAL_UART_Transmit(NET_UART, (uint8_t *)cmd, strlen(cmd));
    HAL_Delay(1);
    Drv_Buffer_Clean(&CmdRetBuffer);
    HAL_UART_Transmit(NET_UART, (uint8_t *)socket, len);
    while(timeout != 0)
    {
        if(Drv_Buffer_Read(&CmdRetBuffer, (uint8_t*)&buf[i]) == 0)
        {
            i = (i+1)%64;
            if(strstr(buf, "SEND OK") != 0)
            {
                return 0;
            }
        }
        else
        {
            timeout--;
            HAL_Delay(1);
        }
    }
    return -1;
}

int Drv_Net_RecvSocket(char *buf, int len, int timeout)
{
    static int tmp = 0;
    tmp += Drv_Buffer_ReadBytes(&NetDataBuffer, (uint8_t*)&buf[tmp], len);
    if(tmp == len)
    {
        tmp = 0;
        return len;
    }
    return tmp;
}

int Drv_Net_ConnectWiFi(const char *ssid, const char *pwd, int timeout)
{
    char buf[50] = "AT+CWJAP_CUR=\"";
    strcat(buf, ssid);
    strcat(buf, "\",\"");
    strcat(buf, pwd);
    strcat(buf, "\"");
    return Drv_Net_TransmitCmd(buf, "WIFI GOT IP\r\n", timeout);
}

int Drv_Net_DisconnectWiFi(void)
{
    return Drv_Net_TransmitCmd("AT+CWQAP", "OK\r\n", 500);
}

int Drv_Net_ConnectTCP(const char *ip, int port, int timeout)
{
    int ret = -1;
    char buf[128] = "AT+CIPSTART=\"TCP\",\"";
    sprintf(&buf[19], "%s\",%d", ip, port);
    ret = Drv_Net_TransmitCmd(buf, "CONNECT\r\n\r\nOK\r\n", timeout);
    if(ret != 0)
    {
        Drv_Net_Disconnect_TCP_UDP();
        HAL_Delay(10);
        ret = Drv_Net_TransmitCmd(buf, "CONNECT\r\n\r\nOK\r\n", timeout);
    }
    
    return ret;
}

int Drv_Net_Disconnect_TCP_UDP(void)
{
    return Drv_Net_TransmitCmd("AT+CIPCLOSE", "OK\r\n", 500);
}

enum AT_STATUS {
	INIT_STATUS,
	LEN_STATUS,
	DATA_STATUS
};
static uint8_t g_DataBuff[256] = {0};
void NetDataProcess_Callback(uint8_t data)
{
    uint8_t *buf = g_DataBuff;
    static enum AT_STATUS g_status = INIT_STATUS;
    static int g_DataBuffIndex = 0;
    static int g_DataLen = 0;
    int i = g_DataBuffIndex;
    int m = 0;
    
    buf[i] = data;
    g_DataBuffIndex++;
    
    switch(g_status)
    {
        case INIT_STATUS:
		{
			if (buf[0] != '+')
			{
				g_DataBuffIndex = 0;
			}			
			else if (i == 4)
			{
				if (strncmp((char*)buf, "+IPD,", 5) == 0)
				{
					g_status = LEN_STATUS;
				}
				g_DataBuffIndex = 0;
			}
			break;
		}

		case LEN_STATUS:
		{
			if (buf[i] == ':')
			{
				/* 计算长度 */
				for (m = 0; m < i; m++)
				{
					g_DataLen = g_DataLen * 10 + buf[m] - '0';
				}
				g_status = DATA_STATUS;
				g_DataBuffIndex = 0;
			}
			else if (i >= 9)  /* ESP8266数据buffer大小是2920,  4位数: +IPD,YYYY:xxxxxx */
			{
				/* 都没有接收到':' */
				g_status = INIT_STATUS;
				g_DataBuffIndex = 0;
			}
			break;
		}

		case DATA_STATUS:
		{
			if (g_DataBuffIndex == g_DataLen)
			{
                /* 接收完数据 */
				Drv_Buffer_WriteBytes(&NetDataBuffer, buf, g_DataLen);
                /* 恢复初始状态 */
				g_status = INIT_STATUS;
				g_DataBuffIndex = 0;
				g_DataLen = 0;
			}
			break;
		}
        default:break;
    }
}

