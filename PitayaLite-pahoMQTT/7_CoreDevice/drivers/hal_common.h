/*
 * Copyright 2021 MindMotion Microelectronics Co., Ltd.
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __HAL_COMMON_H__
#define __HAL_COMMON_H__

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "hal_device_registers.h"

typedef enum
{
  HAL_OK       = 0x00U,
  HAL_ERROR    = 0x01U,
  HAL_BUSY     = 0x02U,
  HAL_TIMEOUT  = 0x03U
} HAL_StatusTypeDef;

#endif /* __HAL_COMMON_H__ */

