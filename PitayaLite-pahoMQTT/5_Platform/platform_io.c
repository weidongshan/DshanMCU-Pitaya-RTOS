#include "platform_io.h"

#include <drv_led.h>
#include <drv_key.h>
#include <drv_uart.h>

void platform_io_init(struct IODev *dev)
{
    if(dev == NULL) return;
    
    switch(dev->Type)
    {
        case LED:
        {
            Drv_LED_Init();
            break;
        }
        case KEY:
        {
            Drv_Key_Init();
            break;
        }
        case DBGOUT:
        {
            Drv_UART_Init();
            break;
        }
        default:break;
    }
}

int platform_io_write(struct IODev *dev, uint8_t *buf, uint16_t len)
{
    if(dev == NULL || buf==NULL || len==0) return -1;
    
    return Drv_LED_Write(buf[0]);
}

int platform_io_read(struct IODev *dev, uint8_t *buf, uint16_t len)
{
    if(dev == NULL || buf==NULL || len==0) return -1;
    
    return Drv_Key_Read(buf, len);
}

