#ifndef __APP_MQTT_H
#define __APP_MQTT_H

typedef struct{
    char *product_id;
    char *dev_name;
    char *dev_secret;
}DeviceInfo_t;

typedef struct{
    char *clientid;
    char *username;
    char *password;
}MQTTInfo_t;

#endif /* __APP_MQTT_H */
