#include "drv_timer.h"
#include "drv_sysclk.h"
#include "hal_tim_16b.h"

static volatile uint32_t uwTick = 0;
extern void HAL_IncTick(void);
extern unsigned int HAL_GetTick(void);
extern void Drv_Key_ProcessCallBack(uint32_t tick);

void Drv_TIM_BaseInit(void)
{
    uint32_t prioritygroup = 0x00U;
    /* Set the counter counting step. */
    TIM_16B_Init_Type tim_init;
    
    RCC_EnableAPB1Periphs(RCC_APB1_PERIPH_TIM3, true);
    
    tim_init.ClockFreqHz = HAL_GetSysClockFreq();
    tim_init.StepFreqHz = 1000000;
    tim_init.Period = 1000 - 1;
    tim_init.EnablePreloadPeriod = false;
    tim_init.PeriodMode = TIM_16B_PeriodMode_Continuous;
    tim_init.CountMode = TIM_16B_CountMode_Increasing;
    TIM_16B_Init(TIM3, &tim_init);

    /* Enable interrupt. */
    NVIC_EnableIRQ(TIM3_IRQn);
    prioritygroup = NVIC_GetPriorityGrouping();
    NVIC_SetPriority(TIM3_IRQn, NVIC_EncodePriority(prioritygroup, 15, 0));
    TIM_16B_EnableInterrupts(TIM3, TIM_16B_INT_UPDATE_PERIOD, true);

    /* Start the counter. */
    TIM_16B_Start(TIM3);
}

/* TIM_BASIC Period timeout ISR. */
void TIM3_IRQHandler(void)
{
    uint32_t flags = TIM_16B_GetInterruptStatus(TIM3);
    if ( 0u != (flags & TIM_16B_STATUS_UPDATE_PERIOD ) ) /* Check update status. */
    {
        HAL_IncTick();
        Drv_Key_ProcessCallBack(HAL_GetTick());
    }
    TIM_16B_ClearInterruptStatus(TIM3, flags);
}