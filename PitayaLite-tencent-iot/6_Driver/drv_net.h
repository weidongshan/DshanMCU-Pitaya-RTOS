#ifndef __DRV_NET_H
#define __DRV_NET_H

#define UART2_TX_PIN            GPIO_PIN_2
#define UART2_RX_PIN            GPIO_PIN_3
#define UART2_PORT              GPIOA
#define UART2_GPIO_CLK_EN()     RCC_EnableAHB1Periphs(RCC_AHB1_PERIPH_GPIOA, true)

#define NET_UART                UART2
#define NET_UART_CLK_EN()       RCC_EnableAPB1Periphs(RCC_APB1_PERIPH_UART2, true)

int Drv_Net_Init(void);
int Drv_Net_TransmitSocket(const char *socket, int len, int timeout);
int Drv_Net_RecvSocket(char *buf, int len, int timeout);
int Drv_Net_ConnectWiFi(const char *ssid, const char *pwd, int timeout);
int Drv_Net_DisconnectWiFi(void);
int Drv_Net_ConnectTCP(const char *ip, int port, int timeout);
int Drv_Net_Disconnect_TCP_UDP(void);


#endif /* __DRV_NET_H */
