#include "hal_common.h"
#include "drv_sysclk.h"

static volatile uint32_t uwTick = 0;

int Drv_SysTick_ConfigUseREG(void)
{
    SYSTICK->RVR = HAL_GetSysClockFreq()/1000 - 1;
    SYSTICK->CVR = 0;
    SYSTICK->CSR |= (1<<0) |    // Enable counter
                    (1<<1) |    // Enable Interupt
                    (1<<2);     // Clock:cpu clock
        
    /* Configure the SysTick IRQ priority */
    uint32_t prioritygroup = 0x00U;
    prioritygroup = NVIC_GetPriorityGrouping();
    NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(prioritygroup, 15, 0));
    
    return 0;
}

int Drv_SysTick_Config(void)
{
    uint32_t prioritygroup = 0x00U;
    uint32_t SystemCoreClock = HAL_GetSysClockFreq();
    /* Configure the SysTick to have interrupt in 1ms time basis*/
    if(SysTick_Config(SystemCoreClock/1000) > 0)
    {
        return -1;
    }
    /* Configure the SysTick IRQ priority */
    prioritygroup = NVIC_GetPriorityGrouping();
    NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(prioritygroup, 15, 0));
    
    return 0;
}

void HAL_IncTick(void)
{
    uwTick += 1;
}

__WEAK void SysTick_Handler(void)
{
    HAL_IncTick();
}

unsigned int HAL_GetTick(void)
{
    return uwTick;
}

void HAL_Delay(unsigned int Delay)
{
    volatile uint32_t tickstart = HAL_GetTick();
    volatile uint32_t wait = Delay;

    /* Add a freq to guarantee minimum wait */
    if (wait < 0xFFFFFFFFU)
    {
        wait += (uint32_t)(1);
    }

    while ((HAL_GetTick() - tickstart) < wait)
    {
    }
}