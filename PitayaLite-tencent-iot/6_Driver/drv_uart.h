#ifndef __DRIVER_UART_H
#define __DRIVER_UART_H

#define UART1_TX_PIN            GPIO_PIN_6
#define UART1_RX_PIN            GPIO_PIN_7
#define UART1_PORT              GPIOB
#define UART1_GPIO_CLK_EN()     RCC_EnableAHB1Periphs(RCC_AHB1_PERIPH_GPIOB, true)

#define DBG_UART                UART1
#define DBG_UART_CLK_EN()       RCC_EnableAPB2Periphs(RCC_APB2_PERIPH_UART1, true)

void Drv_UART_Init(void);

#endif /* __DRIVER_UART_H */
