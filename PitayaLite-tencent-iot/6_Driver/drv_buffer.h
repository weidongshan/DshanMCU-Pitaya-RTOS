#ifndef __DRV_BUFFER_H
#define __DRV_BUFFER_H

#include "hal_common.h"

typedef struct{
    uint8_t *fifo;
    uint16_t pw;
    uint16_t pr;
    uint16_t buf_size;
}RingBuffer, *ptRingBuffer;

/*
    函数名：Driver_Buffer_Init
    功能：初始化一个指定的环形缓冲区
    输入参数：buffer->指向目标缓冲区；size->表示缓冲区分配的内存大小，单位是字节
    输出参数：无
    返回值：-1->表示错误；0->表示成功
*/
int Drv_Buffer_Init(ptRingBuffer buffer, uint16_t size);

/*
    函数名：Driver_Buffer_Write
    功能：往指定的环形缓冲区写入一个字节的数据
    输入参数：buffer->指向目标缓冲区；data->写入的数据
    输出参数：无
    返回值：-1->表示错误；0->表示成功
*/
int Drv_Buffer_Write(ptRingBuffer buffer, const uint8_t data);

/*
    函数名：Driver_Buffer_WriteBytes
    功能：往指定的环形缓冲区写入多个字节的数据
    输入参数：buffer->指向目标缓冲区；data_stream->写入的数据流；len->写入的数据个数，单位是字节
    输出参数：无
    返回值：-1->表示错误；0->表示成功
*/
int Drv_Buffer_WriteBytes(ptRingBuffer buffer, const uint8_t *data_stream, uint8_t len);

/*
    函数名：Driver_Buffer_Read
    功能：从指定的环形缓冲区读出一个字节的数据
    输入参数：buffer->指向目标缓冲区；
    输出参数：data->读出的数据
    返回值：-1->表示错误；0->表示成功
*/
int Drv_Buffer_Read(ptRingBuffer buffer, uint8_t *data);

/*
    函数名：Driver_Buffer_ReadBytes
    功能：往指定的环形缓冲区读出多个字节的数据
    输入参数：buffer->指向目标缓冲区；len->读出的数据个数，单位是字节
    输出参数：data_stream->读出的数据流
    返回值：-1->表示错误；0->表示成功
*/
int Drv_Buffer_ReadBytes(ptRingBuffer buffer, uint8_t *data_stream, uint8_t len);

/*
    函数名：Driver_Buffer_Clean
    功能：清除指定的环形缓冲区
    输入参数：buffer->指向目标缓冲区
    输出参数：无
    返回值：-1->表示错误；0->表示成功
*/
int Drv_Buffer_Clean(ptRingBuffer buffer);

#endif /* __DRV_BUFFER_H */

