# 1. 前言

​	本篇将会从FreeRTOS官网下载内核源码，移植到MM32F320处理器的工程中，验证方法是创建一个任务，在任务中闪烁LED。<!--more-->

# 2. 移植

## 2.1 获取源码

* [FreeRTOS](https://www.freertos.org)
* 下载版本：

![](pic/1_rots_version.jpg)


选择LTS版本。

## 2.2 移植源文件

* 将下载下来的源码copy到工程目录的【4_Middleware】文件夹下；

![](pic/2_copy_source_file.jpg)


* 添加根目录下的源文件到工程：

![](pic/3_root_c_file.jpg)


* 添加port的源文件到工程：

这里需要注意的是，要根据自己的编译环境来选择，比如用的armclang，那么则选择RVDS下的port.c；而如果是gcc，那么则选择GCC下的port.c。我们移植的MM32 MindSDK库，是使用的ARM Compiler 6编译器，用的是GCC下的port.c：

![](pic/4_port_c.jpg)

![](pic/5_port_c_in_prj.jpg)


* 添加内存管理文件

![](pic/6_mem_file.jpg)

我们建议使用heap_4或者heap_5，我们工程中使用的是heap_4：

![](pic/7_mem_file_in_prj.jpg)


* 设置FreeRTOS的配置文件FreeRTOSConfig.h:

这个文件在内核源码中是不存在的，需要我们自己创建保存，我们将其创建保存在【4_Middleware】的根目录下：

![](pic/8_freertos_config_h.jpg)


这个头文件的内容是根据官方提供的参考，然后再根据自己使用的处理器以及应用场景来配置的，内容如下：

```C
#ifndef __FREERTOSCONFIG_H
#define __FREERTOSCONFIG_H

/* Here is a good place to include header files that are required across your application. */ 
#include "hal_common.h"

#define vPortSVCHandler 	    SVC_Handler 
#define xPortPendSVHandler 	    PendSV_Handler 
#define xPortSysTickHandler 	SysTick_Handler 

#define configUSE_PREEMPTION                    1 
#define configUSE_PORT_OPTIMISED_TASK_SELECTION 0 
#define configUSE_TICKLESS_IDLE                 0 
#define configCPU_CLOCK_HZ                      (72000000) 
//#define configSYSTICK_CLOCK_HZ                  (9000000) 
#define configTICK_RATE_HZ                      (1000) 
#define configMAX_PRIORITIES                    15 
#define configMINIMAL_STACK_SIZE                128 
#define configMAX_TASK_NAME_LEN                 16 
#define configUSE_16_BIT_TICKS                  0 
#define configIDLE_SHOULD_YIELD                 1 
#define configUSE_TASK_NOTIFICATIONS            1 
#define configTASK_NOTIFICATION_ARRAY_ENTRIES   3 
#define configUSE_MUTEXES                       1 
#define configUSE_RECURSIVE_MUTEXES             0 
#define configUSE_COUNTING_SEMAPHORES           0 
#define configUSE_ALTERNATIVE_API               0 /* Deprecated! */ 
#define configQUEUE_REGISTRY_SIZE               10 
#define configUSE_QUEUE_SETS                    0 
#define configUSE_TIME_SLICING                  0 
#define configUSE_NEWLIB_REENTRANT              0 
#define configENABLE_BACKWARD_COMPATIBILITY     0 
#define configNUM_THREAD_LOCAL_STORAGE_POINTERS 5 
#define configSTACK_DEPTH_TYPE                  uint16_t 
#define configMESSAGE_BUFFER_LENGTH_TYPE        size_t 

/* Memory allocation related definitions. */ 
#define configSUPPORT_STATIC_ALLOCATION         0 
#define configSUPPORT_DYNAMIC_ALLOCATION        1 
#define configTOTAL_HEAP_SIZE                   (1024*10) 
#define configAPPLICATION_ALLOCATED_HEAP        0 
#define configSTACK_ALLOCATION_FROM_SEPARATE_HEAP 1 

/* Hook function related definitions. */ 
#define configUSE_IDLE_HOOK                     0 
#define configUSE_TICK_HOOK                     0 
#define configCHECK_FOR_STACK_OVERFLOW          0 
#define configUSE_MALLOC_FAILED_HOOK            0 
#define configUSE_DAEMON_TASK_STARTUP_HOOK      0 

/* Run time and task stats gathering related definitions. */ 
#define configGENERATE_RUN_TIME_STATS           0 
#define configUSE_TRACE_FACILITY                0 
#define configUSE_STATS_FORMATTING_FUNCTIONS    0 

/* Co-routine related definitions. */ 
#define configUSE_CO_ROUTINES                   0 
#define configMAX_CO_ROUTINE_PRIORITIES         1 
/* Software timer related definitions. */ 
#define configUSE_TIMERS                        1           
#define configTIMER_TASK_PRIORITY               5  
#define configTIMER_QUEUE_LENGTH                10 
#define configTIMER_TASK_STACK_DEPTH            configMINIMAL_STACK_SIZE 
/* Interrupt nesting behaviour configuration. */ 
#define configKERNEL_INTERRUPT_PRIORITY         255
#define configMAX_SYSCALL_INTERRUPT_PRIORITY    191
#define configMAX_API_CALL_INTERRUPT_PRIORITY   191

/* Define to trap errors during development. */ 
//#define configASSERT( ( x ) ) if( ( x ) == 0 ) vAssertCalled( __FILE__, __LINE__ ) 

/* FreeRTOS MPU specific definitions. */ 
#define configINCLUDE_APPLICATION_DEFINED_PRIVILEGED_FUNCTIONS  0 
#define configTOTAL_MPU_REGIONS                                 8 /* Default value. */ 
#define configTEX_S_C_B_FLASH                                   0x07UL /* Default value. */ 
#define configTEX_S_C_B_SRAM                                    0x07UL /* Default value. */ 
#define configENFORCE_SYSTEM_CALLS_FROM_KERNEL_ONLY             1 
#define configALLOW_UNPRIVILEGED_CRITICAL_SECTIONS              1 

/* ARMv8-M secure side port related definitions. */ 
#define secureconfigMAX_SECURE_CONTEXTS                         5 

/* Optional functions - most linkers will remove unused functions anyway. */ 
#define INCLUDE_vTaskPrioritySet            1 
#define INCLUDE_uxTaskPriorityGet           1 
#define INCLUDE_vTaskDelete                 1 
#define INCLUDE_vTaskSuspend                1 
#define INCLUDE_xResumeFromISR              1 
#define INCLUDE_vTaskDelayUntil             1 
#define INCLUDE_vTaskDelay                  1 
#define INCLUDE_xTaskGetSchedulerState      1 
#define INCLUDE_xTaskGetCurrentTaskHandle   1 
#define INCLUDE_uxTaskGetStackHighWaterMark 0 
#define INCLUDE_xTaskGetIdleTaskHandle      0 
#define INCLUDE_eTaskGetState               0 
#define INCLUDE_xEventGroupSetBitFromISR    1 
#define INCLUDE_xTimerPendFunctionCall      0 
#define INCLUDE_xTaskAbortDelay             0 
#define INCLUDE_xTaskGetHandle              0 
#define INCLUDE_xTaskResumeFromISR          1 


#endif /* __FREERTOSCONFIG_H */
```

## 2.3 设置编译路径

要想编译器找到FreeRTOS内核的相关头文件，就需要将这些头文件的所在路径在mdk工程中添加进去：

![](pic/9_freertos_path.jpg)


设置好编译路径后先编译一下，确保编译FreeRTOS的源文件没有问题。

# 3. 验证

## 3.1 编写驱动文件

​	我们需要配置MM32F3270的时钟，然后还需要控制LED来验证移植，所以还需要写一个LED的驱动；我们还添加了一个调试串口的驱动，都放到了源代码中。

## 3.2 创建任务

​	由于在FreeRTOSConfig.h中我们没有选择静态分配内存：

```C
#define configSUPPORT_STATIC_ALLOCATION         0 
#define configSUPPORT_DYNAMIC_ALLOCATION        1 
#define configTOTAL_HEAP_SIZE                   (1024*10) 
#define configAPPLICATION_ALLOCATED_HEAP        0 
#define configSTACK_ALLOCATION_FROM_SEPARATE_HEAP 1 
```

所以只能调用动态分配的那个任务创建函数：`xTaskCreate`：

```C
BaseType_t xTaskCreate( TaskFunction_t pxTaskCode,					// 任务的入口函数
						const char * const pcName, 					// 任务的名称
                        const configSTACK_DEPTH_TYPE usStackDepth,	// 分配给任务的栈大小
                        void * const pvParameters,					// 传递给任务的参数
                        UBaseType_t uxPriority,						// 分配给任务的优先级
                        TaskHandle_t * const pxCreatedTask )		// 返回的任务句柄值
```

这个函数的返回值：

```C
pdPASS = 创建成功
errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY = 创建失败
```

根据任务参数的描述，我们需要一个任务的入口函数，形式如：`void TestCode(void *parameter)`，然后创建一个任务：

```c
xTaskCreate(TestCode, \	// 任务的入口函数
            "Test", \	// 任务的名称
            128, \		// 分配给任务的栈大小:word
            NULL, \		// 传递给任务的参数
            1, \		// 分配给任务的优先级
            NULL);		// 返回的任务句柄值
```

## 3.3 任务函数

​	我们对于任务的示例函数的实现如下：

```C
void TestCode(void *parameter)
{
    volatile uint32_t cnt = 0;
    printf("\r\nTest Task\r\n");
    while(1)
    {
        Driver_LED_Write(RedLED, LED_ON);
        vTaskDelay(500);
        Driver_LED_Write(RedLED, LED_OFF);
        vTaskDelay(500);
        printf("LED task running :%d\r", cnt++);
    }
}
```

在里面闪烁LED，并打印运行次数信息，大约每隔1s打印一次。

## 3.4 开启任务调度

* 接口：`void vTaskStartScheduler( void )`

当调用这个函数后，正常情况下这个函数之后的代码都不会运行到，比如：

```C
int main(void)
{
    volatile uint32_t cnt = 0;
    SystemClockConfig();
    Driver_SysTick_Config();
    Driver_LED_Init();
    DBG_UART_Init();
    
    xTaskCreate(TestCode, "Test", 128, NULL, 1, NULL);
    vTaskStartScheduler();
    
    printf("Hello China.\r\n");
    
    while(1)
    {
        
    }
    
    return 0;
}
```

这个代码中【Hello China.】就不会被打印出来。

## 3.5 现象

![](pic/10_result.jpg)

![](pic/11_result_2.jpg)


​	开发板运行这个例程已经有一天多的时间了，从这个计数值也可以看出来，说明移植FreeRTOS到MM32基本没有问题了。
