#ifndef __DRIVER_SYSTICK_H
#define __DRIVER_SYSTICK_H

int Driver_SysTick_Config(void);
unsigned int HAL_GetTick(void);
void HAL_Delay(unsigned int Delay);

#endif /* __DRIVER_SYSTICK_H */
