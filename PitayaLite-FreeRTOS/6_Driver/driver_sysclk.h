#ifndef __DRIVER_SYSCLK_H
#define __DRIVER_SYSCLK_H

#include "hal_rcc.h"

#define SYSTICK_BASE_ADDR   (0xE000E010)
typedef struct{
    unsigned int CSR;
    unsigned int RVR;
    unsigned int CVR;
    unsigned int CALIB;
}SYSTICK_REG;

#define SYSTICK             ((SYSTICK_REG*)SYSTICK_BASE_ADDR)

#define LSI_VALUE           (40000)
#define HSI_VALUE           (8000000)
#define HSE_VALUE           (12000000)

#define __USE_HSE           (0)
#define __USE_HSE_PLL       (1)
#define __USE_HSI_PLL       (0)

#define RCC_CFGR_SWS_Pos                     (2U)                              
#define RCC_CFGR_SWS_Msk                     (0x3UL << RCC_CFGR_SWS_Pos)        /*!< 0x0000000C */

#define RCC_CFGR_SWS_HSI                     0x00000000U                       /*!< HSI oscillator used as system clock */
#define RCC_CFGR_SWS_HSE                     0x00000004U                       /*!< HSE oscillator used as system clock */
#define RCC_CFGR_SWS_PLL                     0x00000008U                       /*!< PLL used as system clock */
#define RCC_CFGR_SWS_LSI                     0x0000000CU                       /*!< LSI used as system clock */

#define RCC_SYSCLKSOURCE_STATUS_HSI         RCC_CFGR_SWS_HSI            /*!< HSI used as system clock */
#define RCC_SYSCLKSOURCE_STATUS_HSE         RCC_CFGR_SWS_HSE            /*!< HSE used as system clock */
#define RCC_SYSCLKSOURCE_STATUS_PLLCLK      RCC_CFGR_SWS_PLL            /*!< PLL used as system clock */
#define RCC_SYSCLKSOURCE_STATUS_LSI         RCC_CFGR_SWS_LSI            /*!< LSI used as system clock */

uint32_t HAL_GetSysClockFreq(void);
uint32_t HAL_Get_AHB_Clock(void);
uint32_t HAL_Get_APB1_Clock(void);
uint32_t HAL_Get_APB2_Clock(void);
void SystemClock_Config(void);

#endif /* __DRIVER_SYSCLK_H */
