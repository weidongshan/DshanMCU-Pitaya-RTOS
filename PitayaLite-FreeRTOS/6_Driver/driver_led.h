/* driver_led.h */

#ifndef __DRIVER_LED_H
#define __DRIVER_LED_H

#define USER_LED_PIN            GPIO_PIN_1
#define USER_LED_PORT           GPIOA
#define USER_LED(STATUS)        GPIO_WriteBit(USER_LED_PORT, USER_LED_PIN, STATUS)

#define USER_LED_GPIO_CLK_EN()       RCC_EnableAHB1Periphs(RCC_AHB1_PERIPH_GPIOA, true)

typedef enum{
    LED_ON  = 0,
    LED_OFF
}LED_Status;
                             
typedef enum{
    UserLed  = 0,
    Reserved
}LED_Type;

void Driver_LED_Init(void);
void Driver_LED_Write(LED_Type type, LED_Status status);

#endif /* __DRIVER_LED_H */
