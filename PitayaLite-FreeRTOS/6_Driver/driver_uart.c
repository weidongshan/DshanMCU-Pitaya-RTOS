#include "driver_uart.h"
#include "driver_sysclk.h"
#include "hal_rcc.h"
#include "hal_gpio.h"
#include "hal_uart.h"

#include <stdio.h>

static void DBG_UART_GPIO_Init(void)
{
    GPIO_Init_Type gpio_init;
    
    UART1_GPIO_CLK_EN();
    /* PA9  - UART1_TX. */
    /* PA10 - UART1_RX. */
    gpio_init.Pins  = UART1_TX_PIN;
    gpio_init.PinMode  = GPIO_PinMode_AF_PushPull;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(UART1_PORT, &gpio_init);
    GPIO_PinAFConf(UART1_PORT, gpio_init.Pins, GPIO_AF_7);
    
    gpio_init.Pins  = UART1_RX_PIN;
    gpio_init.PinMode  = GPIO_PinMode_In_Floating;
    gpio_init.Speed = GPIO_Speed_50MHz;
    GPIO_Init(UART1_PORT, &gpio_init);
    GPIO_PinAFConf(UART1_PORT, gpio_init.Pins, GPIO_AF_7);
}

void DBG_UART_Init(void)
{
    UART_Init_Type uart_init;
    
    DBG_UART_CLK_EN();
    DBG_UART_GPIO_Init();

    uart_init.ClockFreqHz   = HAL_Get_APB2_Clock();
    uart_init.BaudRate      = 115200;
    uart_init.WordLength    = UART_WordLength_8b;
    uart_init.StopBits      = UART_StopBits_1;
    uart_init.Parity        = UART_Parity_None;
    uart_init.XferMode      = UART_XferMode_RxTx;
    uart_init.HwFlowControl = UART_HwFlowControl_None;
    UART_Init(DBG_UART, &uart_init);
    UART_Enable(DBG_UART, true);
}

#if defined (__ARMCC_VERSION) && (__ARMCC_VERSION < 6100100)
struct __FILE{
    int handle;
};
#endif

FILE __stdout;
int fputc(int ch, FILE *f) 
{
    /* Your implementation of fputc(). */
    while ( 0u == (UART_STATUS_TX_EMPTY & UART_GetStatus(DBG_UART)) )
    {}
    UART_PutData(DBG_UART, (uint8_t)(ch));
    return ch;
}

