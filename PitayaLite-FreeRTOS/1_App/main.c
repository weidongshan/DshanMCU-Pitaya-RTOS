#include "driver_led.h"
#include "driver_sysclk.h"
#include "driver_systick.h"
#include "driver_uart.h"

#include "FreeRTOS.h"
#include "task.h"

#include <stdio.h>

void TestCode(void *parameter)
{
    volatile uint32_t cnt = 0;
    printf("\r\nTest Task\r\n");
    while(1)
    {
        Driver_LED_Write(UserLed, LED_ON);
        vTaskDelay(500);
        Driver_LED_Write(UserLed, LED_OFF);
        vTaskDelay(500);
        printf("LED task running :%d\r", cnt++);
    }
}

int main(void)
{
    volatile uint32_t cnt = 0;
    SystemClock_Config();
    Driver_LED_Init();
    DBG_UART_Init();
    
    printf("Hello China.\r\n");
    
    xTaskCreate(TestCode, "Test", 128, NULL, 1, NULL);
    vTaskStartScheduler();
    
    printf("Hello China.\r\n");
    
    while(1)
    {
        
    }
    
    return 0;
}
