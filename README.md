# 更新记录
| 日期 | 更改说明 | 更改人 |
|-|-|-|
|2022/9/1|提交初版程序|Alen|
|2022/9/10|更改工程默认芯片，增加移植FreeRTOS到MM32简要说明|Alen|
|2022/9/29|更改FreeRTOSConfig.h中CPU时钟，由72MHz改为120MHz|Alen|
|2022/10/23|增加MQTT工程，使用的是paho MQTT|Alen|
|2022/12/08|增加连接腾讯物联网平台的MQTT工程，使用的是paho MQTT|Alen|

## PitayaLite_RT-Thread
基于RT-Thread Env工具配置的火龙果rtt工程。

---

## PitayaLite-FreeRTOS
基于freeRTOS Kernel。

---

## PitayaLite-pahoMQTT
* paho MQTT
* freeRTOS
* ESP8266
* UART2(PA2-TX, PA3-RX)
* WiFi名称和密码设置在MQTTFreeRTOS.h中的宏定义
```
#define SSID    ""
#define PWD     ""
```

* MQTT连接的是阿里云物联网平台，参数在app_mqtt.c中设置
```
const static char mqttHostUrl[] = "";
const static char clientID[] = "";
const static char username[] = "";
const static char password[] = "";

const static char LedTopic[] = "";
const static char KeyTopic[] = "";
```
* 现象

![现象1](pic/pic1.png)
![现象2](pic/pic2.png)

---

## PitayaLite-tencent-iot
* cJSON
* paho MQTT
* freeRTOS
* ESP8266
* UART2(PA2-TX, PA3-RX)
* WiFi名称和密码设置在MQTTFreeRTOS.h中的宏定义
```
#define SSID    ""
#define PWD     ""
```
* MQTT连接的是腾讯物联网平台，参数在app_mqtt.c中设置。
* 完成的功能是使用微信小程序“腾讯连连”控制开发板的LED，获取开发板上按键的按下时长，做一个建议的物联网小项目。
* 具体的文档会抽空整理。